import { RouterModule, Routes } from '@angular/router';
import { PrimosComponent } from './components/primos/primos.component';
import { PrimosRelacionalComponent } from './components/primos-relacional/primos-relacional.component';
import { GemelosComponent } from './components/gemelos/gemelos.component';
import { GemelosRelacionalComponent } from './components/gemelos-relacional/gemelos-relacional.component';


const APP_ROUTES: Routes = [
    { path: 'numeros-primos', component: PrimosComponent },
    { path: 'numeros-primos-relacional', component: PrimosRelacionalComponent },
    { path: 'numeros-gemelos', component: GemelosComponent },
    { path: 'numeros-gemelos-relacional', component: GemelosRelacionalComponent },
    { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
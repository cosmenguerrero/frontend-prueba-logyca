import { Component, OnInit } from '@angular/core';
import { Gemelos } from 'src/app/model/gemelos';
import Swal from 'sweetalert2';
import { ApiCalculoService } from 'src/app/services/api-calculo.service';
import { Router } from '@angular/router';
import { Numeros } from 'src/app/model/numeros';

@Component({
  selector: 'app-gemelos',
  templateUrl: './gemelos.component.html',
  styleUrls: ['./gemelos.component.css'],
})
export class GemelosComponent implements OnInit {
  listaNumerosGemelos: Array<Gemelos>;
  numeros: Numeros;

  constructor(
    private calculoServices: ApiCalculoService,
    private router: Router
  ) {
    this.numeros = new Numeros();
  }

  ngOnInit(): void {}

  //Consumir servicio del Api Numeros Gemelos
  getListaNumerosGemelos(n: number) {
    if (n > 0) {
      this.calculoServices.getListarNumerosGemelos(n).subscribe((response) => {
        this.listaNumerosGemelos = response;
      });
    } else {
      Swal.fire('Error!', 'Recuerda que el numero debe ser positivo!', 'error');
      this.numeros.numeros = null;
    }
  }
}

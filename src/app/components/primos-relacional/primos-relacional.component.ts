import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCalculoService } from 'src/app/services/api-calculo.service';
import { Numeros } from 'src/app/model/numeros';
import { Primos } from 'src/app/model/primos';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-primos-relacional',
  templateUrl: './primos-relacional.component.html',
  styleUrls: ['./primos-relacional.component.css']
})
export class PrimosRelacionalComponent implements OnInit {
  listaNumerosPrimosRel: Array<Primos>;
  numeros: Numeros;

  constructor(private calculoServices: ApiCalculoService,
    private router: Router) {
      this.numeros = new Numeros();
     }

  ngOnInit(): void {
  }

  //Consumir servicio del Api Numeros Primos
  getListaNumerosPrimosRel(n: number) {
    if (n > 0) {
      this.calculoServices.getListarNumerosPrimosRel(n).subscribe((response) => {
        this.listaNumerosPrimosRel = response;
      });
    } else {
      Swal.fire('Error!', 'Recuerda que el numero debe ser positivo!', 'error');
      this.numeros.numeros = null;
    }
  }

}

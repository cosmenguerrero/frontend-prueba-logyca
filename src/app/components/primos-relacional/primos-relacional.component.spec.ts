import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimosRelacionalComponent } from './primos-relacional.component';

describe('PrimosRelacionalComponent', () => {
  let component: PrimosRelacionalComponent;
  let fixture: ComponentFixture<PrimosRelacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimosRelacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimosRelacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

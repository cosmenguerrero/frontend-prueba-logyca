export class Constantes {

    public static HOST = 'http://localhost:8085';

    //LISTA DE URL NUMEROS PRIMOS
    public static LISTAR_NUMEROS_PRIMOS = '/listarnumerosprimos/';
    public static LISTAR_NUMEROS_PRIMOS_RELACIONAL = '/listarnumerosprimosrel/';
  

    //LISTA DE URL NUMEROS GEMELOS
    public static LISTAR_NUMEROS_GEMELOS= '/listarnumerosgemelos/';
    public static LISTAR_NUMEROS_GEMELOS_RELACIONAL = '/listarnumerosgemelosrel/';


}

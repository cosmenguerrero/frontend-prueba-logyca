import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GemelosRelacionalComponent } from './gemelos-relacional.component';

describe('GemelosRelacionalComponent', () => {
  let component: GemelosRelacionalComponent;
  let fixture: ComponentFixture<GemelosRelacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GemelosRelacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GemelosRelacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

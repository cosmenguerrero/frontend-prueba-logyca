import { Component, OnInit } from '@angular/core';
import { Numeros } from 'src/app/model/numeros';
import { Router } from '@angular/router';
import { Primos } from 'src/app/model/primos';
import { ApiCalculoService } from 'src/app/services/api-calculo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-gemelos-relacional',
  templateUrl: './gemelos-relacional.component.html',
  styles: [
  ]
})
export class GemelosRelacionalComponent implements OnInit {

  listaNumerosGemelosRel: Array<Primos>;
  numeros: Numeros;

  constructor(private calculoServices: ApiCalculoService,
    private router: Router) { 
      this.numeros = new Numeros();
    }

  ngOnInit(): void {
  }

    //Consumir servicio del Api Numeros Primos
    getListaGemelosRel(n: number) {
      if (n > 0) {
        this.calculoServices.getListarNumerosGemelosRel(n).subscribe((response) => {
          this.listaNumerosGemelosRel = response;
        });
      } else {
        Swal.fire('Error!', 'Recuerda que el numero debe ser positivo!', 'error');
        this.numeros.numeros = null;
      }
    }

}

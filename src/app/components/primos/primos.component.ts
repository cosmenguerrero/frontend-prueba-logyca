import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCalculoService } from '../../services/api-calculo.service';
import { Primos } from '../../model/primos';
import { Numeros } from '../../model/numeros';
import { Gemelos } from '../../model/gemelos';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-primos',
  templateUrl: './primos.component.html',
  styles: [],
})
export class PrimosComponent implements OnInit {
  listaNumerosPrimos: Array<Primos>;
  numeros: Numeros;

  constructor(
    private calculoServices: ApiCalculoService,
    private router: Router
  ) {
    this.numeros = new Numeros();
  }

  ngOnInit(): void {}

  //Consumir servicio del Api Numeros Primos
  getListaNumerosPrimos(n: number) {
    if (n > 0) {
      this.calculoServices.getListarNumerosPrimos(n).subscribe((response) => {
        this.listaNumerosPrimos = response;
      });
    } else {
      Swal.fire('Error!', 'Recuerda que el numero debe ser positivo!', 'error');
      this.numeros.numeros = null;
    }
  }
}

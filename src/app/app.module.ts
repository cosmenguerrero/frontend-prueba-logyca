import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { PrimosComponent } from './components/primos/primos.component';
import { PrimosRelacionalComponent } from './components/primos-relacional/primos-relacional.component';
import { GemelosComponent } from './components/gemelos/gemelos.component';
import { GemelosRelacionalComponent } from './components/gemelos-relacional/gemelos-relacional.component';

// Rutas
import { APP_ROUTING } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PrimosComponent,
    PrimosRelacionalComponent,
    GemelosComponent,
    GemelosRelacionalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { TestBed } from '@angular/core/testing';

import { ApiCalculoService } from './api-calculo.service';

describe('ApiCalculoService', () => {
  let service: ApiCalculoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiCalculoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

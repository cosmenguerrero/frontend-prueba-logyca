import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Constantes } from "../components/shared/constantes";

@Injectable({
  providedIn: 'root'
})
export class ApiCalculoService {

  constantes: Constantes;

  constructor(private http: HttpClient) { }

  // Conectarse al endpoint Listar Numeros Primos sin base de datos 
    getListarNumerosPrimos(n: number): Observable<any> {
      return this.http.get(Constantes.HOST + Constantes.LISTAR_NUMEROS_PRIMOS + n);
    }

  // Conectarse al endpoint Listar Numeros Gemelos sin base de datos
    getListarNumerosGemelos(n: number): Observable<any> {
      return this.http.get(Constantes.HOST + Constantes.LISTAR_NUMEROS_GEMELOS + n);
    }

    // Conectarse al endpoint Listar Numeros Primos con base de datos 
    getListarNumerosPrimosRel(n: number): Observable<any> {
      return this.http.get(Constantes.HOST + Constantes.LISTAR_NUMEROS_PRIMOS_RELACIONAL + n);
    }

  // Conectarse al endpoint Listar Numeros Gemelos con base de datos
    getListarNumerosGemelosRel(n: number): Observable<any> {
      return this.http.get(Constantes.HOST + Constantes.LISTAR_NUMEROS_GEMELOS_RELACIONAL + n);
    }

 
}

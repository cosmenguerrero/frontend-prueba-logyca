# PruebaLogyca

Este proyecto esta creado en [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.
Esta vista de front contiene:
1. Vista para listar los numeros primos y gemelos basandose en las funciones solicitadas en el punto 1 y 2.
2. El frontend esta conectandose a un backend en Java Spring Boot donde se consumen los Api Rest segun punto 3 y 4
3. La vista está diseñada para consumir los 4 servicios
4. Para la ejecución del proyecto basta con usar `npm install` y se instalaran dependencias tales como Sweet Alert, usado para el manejo de mensajes de numeros negativos. 
5. El front hace las respectivas validaciones para el correcto funcionamiento del aplicativo.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

